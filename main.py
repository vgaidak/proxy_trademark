import re
import string
from http.server import SimpleHTTPRequestHandler
from socketserver import TCPServer
from urllib.error import HTTPError
from urllib.parse import urljoin
from urllib.request import urlopen

from bs4 import BeautifulSoup

PORT = 9099
THRESHOLD = 6
TM_SYMBOL = '\u2122'

BASE_URL = 'https://news.ycombinator.com'


def add_symbol(text: str) -> str:
    """
    adds 'tm' symbol to all strings which have THRESHOLD length but e.g. "people's" won't have 'tm' symbol
    :param text: text from tags
    :return: modified text
    """
    modified_list = []
    word_list = re.split(r'(\s+)', text)  # split with all spaces not to lose them
    for w in word_list:
        # remove all punctuation
        w_without_punc = w.translate(str.maketrans('', '', string.punctuation + '”“'))
        if len(w_without_punc) == THRESHOLD:
            if w_without_punc == w:
                w += TM_SYMBOL
            else:
                if w_without_punc in w:  # if string has punctuation in it, combine new string
                    end_substr_idx = w.find(w_without_punc) + THRESHOLD
                    w = w[:end_substr_idx] + TM_SYMBOL + w[end_substr_idx:]
        modified_list.append(w)
    return ''.join(modified_list)


class MyProxy(SimpleHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html; charset=UTF-8')
        self.end_headers()

    def send_error(self, code, message=None, explain=None):
        if code == 404:
            self.error_message_format = "Unknown."
        SimpleHTTPRequestHandler.send_error(self, code, message)

    def do_GET(self):
        url = urljoin(BASE_URL, self.path)
        try:
            with urlopen(url) as response:
                self._set_headers()
                if self.path == '/' or self.path.startswith('/item?'):
                    request_html = response.read().decode()
                    modified_html = self.modify_html(request_html)
                    self.wfile.write(modified_html.encode())
                else:
                    self.copyfile(response, self.wfile)
        except HTTPError as err:
            self.send_error(err.code)

    def modify_html(self, html: str) -> str:
        soup = BeautifulSoup(html, 'html.parser')

        # todo refactor me
        # change main icon link
        for link in soup.find_all('a', href=True, text=False):
            if link['href'] == BASE_URL:
                link['href'] = '/'
                break

        # find all text
        for element in soup.find_all(['p', 'div', 'a', 'b', 'i', 'span']):
            if element.string:
                element.string = add_symbol(element.get_text())
            elif len(element.contents) > 0:
                for c in element.contents:
                    if c.string:
                        c.string.replace_with(BeautifulSoup(add_symbol(c.string), 'html.parser'))

        return str(soup)


def run_server():
    try:
        TCPServer.allow_reuse_address = True  # solution for `OSError: [Errno 98] Address already in use`
        httpd = TCPServer(('', PORT), MyProxy)
        print(f'Proxy at: http://localhost:{PORT}')
        httpd.serve_forever()
    except KeyboardInterrupt:
        print('Pressed Ctrl+C')
    finally:
        if httpd:
            httpd.shutdown()
            httpd.socket.close()


if __name__ == "__main__":
    run_server()
